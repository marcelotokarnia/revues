# Revues : Reviews with Vue

[How to run locally](./docs/LOCAL_SETUP_EXPLAINED.md)

[How to authenticate and use API properly](./docs/API_EXPLAINED.md)

## About the project

A `Vue` and `Typescript` application in which you can post and query reviews.

This application assets are built with `Webpack`.

All requests are made to `Python / Django` backend through a `Graphql` data layer and stored in a `Postgres` database.

Styling is powered by `Bootstrap@4`

Author: [Marcelo Tokarnia](https://www.github.com/marcelotokarnia)

See the application running on [this video](http://bit.do/revues-app) (Sorry, only Portuguese available).