declare module 'jwt-decode' {
  function decode(token: string): any
  export default decode
}
