import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { ApolloLink, concat, NextLink, Operation } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import Vue from 'vue'
import VueApollo from 'vue-apollo'

const link = new HttpLink({
  fetchOptions: {
    credentials: 'same-origin',
  },
  uri: '/graphql',
})

const authMiddleware = new ApolloLink((operation: Operation, forward?: NextLink) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      Authorization: `JWT ${JWT_TOKEN}`,
    },
  })
  return forward ? forward(operation) : null
  })

const cache = new InMemoryCache()

const apolloClient = new ApolloClient({
  cache,
  connectToDevTools: true,
  link: concat(authMiddleware, link),
})

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

Vue.use(VueApollo)

export default apolloProvider
