import App from '@src/components/App.vue'
import apolloProvider from '@src/vue-configs/vue-apollo'
import Vue from 'vue'

const v = new Vue({
    components: {
        App,
    },
    el: '#app',
    provide: apolloProvider.provide(),
    template: `<App />`,
})
