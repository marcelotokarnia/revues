import graphene
from core.models import Review
from graphql_jwt.decorators import login_required
import graphql_jwt


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class ReviewType(graphene.ObjectType):
    id = graphene.ID()
    ip_address = graphene.String()
    rating = graphene.Float()
    title = graphene.String()
    summary = graphene.String()
    submission_date = graphene.String()
    company = graphene.String()

    def __init__(self, review):
        self.id = review.pk
        self.ip_address = review.ip_address
        self.rating = review.rating
        self.title = review.title
        self.summary = review.summary
        self.submission_date = review.submission_date.isoformat()
        self.company = review.company


class CreateReview(graphene.Mutation):
    class Arguments:
        company = graphene.String()
        summary = graphene.String()
        title = graphene.String()
        rating = graphene.Float()

    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, **args):
        Review.objects.create(
            reviewer=info.context.user,
            ip_address=get_client_ip(info.context),
            rating=args.get('rating'),
            title=args.get('title'),
            summary=args.get('summary'),
            company=args.get('company')
        )
        return CreateReview(ok=True)


class Mutation(graphene.ObjectType):
    create_review = CreateReview.Field()
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()


class Query(graphene.ObjectType):
    reviews = graphene.List(
        ReviewType,
    )

    # requires header Authorization: JWT <token>
    @login_required
    def resolve_reviews(self, info, **args):
        results = []
        for review in info.context.user.reviews.all():
            results.append(ReviewType(review))
        return results


schema = graphene.Schema(mutation=Mutation, query=Query)
