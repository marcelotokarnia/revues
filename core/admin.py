from django.contrib import admin
from core.models import Review
from utils.admin_utils import RawIdAdminModel

admin.site.register(Review, RawIdAdminModel)
