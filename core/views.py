from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login

from graphql_jwt.shortcuts import get_token


def index(request):
    return render(request, 'index.html', {
        'JWT_TOKEN': get_token(request.user, request) if not request.user.is_anonymous else ''
    })


def login_view(request):
    if request.user.is_anonymous:
        return render(request, 'login.html')
    else:
        return render(request, 'index.html', {
            'JWT_TOKEN': get_token(request.user, request)
        })


def authenticate(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    print(username, password)

    user = User.objects.filter(username=username).first()
    if user:
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
        return redirect('/')
    else:
        return render(request, 'login.html', {'invalid': True})
