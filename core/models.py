from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator


def validate_summary_length(value):
    if len(value) > 1e4:
        raise ValidationError(
            'Summary length is {value}, too big. Please submit a summary with less than 10k chars'.format(
                value=value
            )
        )


class Review(models.Model):
    reviewer = models.ForeignKey(User, related_name="reviews", on_delete=models.CASCADE)
    ip_address = models.CharField(max_length=15)
    rating = models.FloatField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    title = models.CharField(max_length=64)
    summary = models.TextField(validators=[validate_summary_length])
    submission_date = models.DateTimeField(auto_now_add=True)
    company = models.CharField(max_length=64)

    def __str__(self):
        return "Review #{id}: User: #{reviewer} about {company} \"{title}\" ({rating} stars) ".format(
            id=self.pk,
            company=self.company,
            reviewer=self.reviewer_id,
            title=self.title,
            rating=self.rating
        )
