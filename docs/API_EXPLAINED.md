# Revues : Reviews with Vue

## The API explained

First, this is a Graphql API, so forget about everything you knew about REST.

On graphql we only have one single endpoint, in our case, `/graphql` and all requests are POST.

So, I suggest you use `GraphiQl` app, but you can also use the good old `Postman`.

If you are using `Postman` you should additionally add a `Content-Type` Header with `application/graphql`.

To authenticate the API requires an `Authentication` Header with `JWT ${token}` value.

In order to get this token you should send a request with the following body:

```
  mutation {
    authToken(username: "${username}", password: "${password}") {
      token
    }
  }
```

With username and password of the user you want to authenticate.

After placing this token on the proper header.

There is mainly two requests you would like to do.

1. Post a review

```
  mutation {
    createReview(company: "${company}", summary: "${summary}", title: "${title}", rating: ${rating}) {
      ok
    }
  }
```

2. Query your reviews

```
  {
    reviews {
      id
      ipAddress
      rating
      title
      summary
      submissionDate
      company
    }
  }
```

Additionally you can also see an UI with your reviews on the homepage (you will be prompted to login if not logged in yet).

Try accessing the main endpoint (`/`), on local server it should be: `http://localhost:8000/`